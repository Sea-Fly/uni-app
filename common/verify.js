import tools from '@/common/tools.js';
/* 验证类 */
export default {
    run: function(obj) {
        for (let i = 0; i < obj.length; i++) {
            let is_empty = true;
            if ((obj[i].canEmpty === false && tools.isEmpty(obj[i].value)) || tools.string.trim(obj[i].value) === '') {
                return {
                    result: false,
                    msg: `${obj[i]['name']}${'不能为空'}`
                };
            } else {
                is_empty = false;
            }

            if (is_empty === false && obj[i]['type'] !== 'notEmpty') {
                let result = false;

                if (obj[i]['type'] === 'reg') {
                    result = this.test['reg'](obj[i].value, obj[i]['reg']);
                } else {
                    result = this.test[obj[i]['type']](obj[i].value);
                }

                if (result === false) {
                    return {
                        result: false,
                        msg: obj[i]['errorMsg']
                    };
                }
            }
        }
        return true;
    },

    /* 验证 */ 
    test: {

        /* 自定义正则验证 */
        reg: function(data, reg) {
            return reg.test(data);
        },

        /* 手机号码 */
        mobile: function(data) {
            let reg = /^1(3[0-9]|4[579]|5[0-3,5-9]|6[6]|7[0135678]|8[0-9]|9[189])\d{8}$/;
            return reg.test(data);
        },
		// 11位手机号码
		mobiles: function(data) {
            let reg = /^1\d{10}$/;
            return reg.test(data);
        },
        /* 邮件 */
        email: function(data) {
            let reg = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/i;
            return reg.test(data);
        },

        /* 身份证 */
        idCart: function(data) {
            let reg = /^(^[1-9]\d{7}((0\d)|(1[0-2]))(([0|1|2]\d)|3[0-1])\d{3}$)|(^[1-9]\d{5}[1-9]\d{3}((0\d)|(1[0-2]))(([0|1|2]\d)|3[0-1])((\d{4})|\d{3}[Xx])$)$/;
            return reg.test(data);
        },

        //身份证验证
        validateIdCard:function(idCard){
            //15位和18位身份证号码的正则表达式
            var regIdCard=/^(^[1-9]\d{7}((0\d)|(1[0-2]))(([0|1|2]\d)|3[0-1])\d{3}$)|(^[1-9]\d{5}[1-9]\d{3}((0\d)|(1[0-2]))(([0|1|2]\d)|3[0-1])((\d{4})|\d{3}[Xx])$)$/;

            //如果通过该验证，说明身份证格式正确，但准确性还需计算
            if(regIdCard.test(idCard)){
                if(idCard.length==18){
                    var idCardWi=new Array( 7, 9, 10, 5, 8, 4, 2, 1, 6, 3, 7, 9, 10, 5, 8, 4, 2 ); //将前17位加权因子保存在数组里
                    var idCardY=new Array( 1, 0, 10, 9, 8, 7, 6, 5, 4, 3, 2 ); //这是除以11后，可能产生的11位余数、验证码，也保存成数组
                    var idCardWiSum=0; //用来保存前17位各自乖以加权因子后的总和
                    for(var i=0;i<17;i++){
                        idCardWiSum+=idCard.substring(i,i+1)*idCardWi[i];
                    }
                    var idCardMod=idCardWiSum%11;//计算出校验码所在数组的位置
                    var idCardLast=idCard.substring(17);//得到最后一位身份证号码
                    //如果等于2，则说明校验码是10，身份证号码最后一位应该是X
                    if(idCardMod==2){
                        if(idCardLast=="X"||idCardLast=="x"){
                            return true;
                        }else{
                            return false;
                        }
                    }else{
                        //用计算出的验证码与最后一位身份证号码匹配，如果一致，说明通过，否则是无效的身份证号码
                        if(idCardLast==idCardY[idCardMod]){
                            return true;
                        }else{
                            return false;
                        }
                    }
                }
            }else{
                return false;
            }
        },

        /* 正整数 */
        positiveInteger: function(data) {
            let reg = /^\+?[0-9][0-9]*$/;
            return reg.test(data);
        },

        /* 邮编 */
        zipCode: function(data) {
            let reg = /^[1-9][0-9]{5}$/;
            return reg.test(data);
        },

        /* 超链接 */
        link: function(data) {
            let strRegex = '^((https|http|ftp|rtsp|mms)?://)' +
                           "?(([0-9a-z_!~*'().&=+$%-]+: )?[0-9a-z_!~*'().&=+$%-]+@)?" + //ftp的user@
                            '(([0-9]{1,3}\.){3}[0-9]{1,3}' + // IP形式的URL- 199.194.52.184
                            '|' + // 允许IP和DOMAIN（域名）
                            "([0-9a-z_!~*'()-]+\.)*" + // 域名- www.
                            '([0-9a-z][0-9a-z-]{0,61})?[0-9a-z]\.' + // 二级域名
                            '[a-z]{2,6})' + // first level domain- .com or .museum
                            '(:[0-9]{1,4})?' + // 端口- :80
                            '((/?)|' + // a slash isn't required if there is no file name
                            "(/[0-9a-z_!~*'().;?:@&=+$,%#-]+)+/?)$";

            let reg = new RegExp(strRegex);
            return reg.test(data);
        }

    }
};
