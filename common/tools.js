import config from '@/config.js';
import errorHnadle from './errorHandle.js';
var jweixin = false;
// #ifdef H5
window.config = config;
jweixin = require('@/common/jwenxin-module'); /* h5分享 */
// #endif

let errorFn = async function(error, option, data) {
	let status = error.statusCode; //状态码
	let error_data = error.data || {}; //错误内容
	let errcode = error_data.error_code;

	if (status === 500) {
		error_data.error_msg = error_data.request_id ? '服务器异常。request_id：' + error_data.request_id : '服务器异常';
	}
	let is_next = true;
	//先执行全局的错误检测,在执行自定义方法
	if (errorHnadle[errcode] && typeof errorHnadle[errcode] == 'function') {
		try {
			is_next = await errorHnadle[errcode](tools, option, error_data);
		} catch (e) {
			console.error(e);
		}
	}
	if (typeof data.errorFun == 'function' && is_next) {
		try {
			setTimeout(function() {
				data.errorFun(error_data, status);
			}, 20);
		} catch (e) {
			console.error(e);
		}
	}
}

let ajaxFailFn = function(error, option, data) {
	const networkType = tools.getNetworkType();
	if (networkType === 'none') {
		setTimeout(function() {
			tools.alert.info('暂无网络, 请检查网络');
		}, 20);
	} else if (error.errMsg.indexOf('timeout') >= 0) {
		setTimeout(function() {
			tools.alert.message('网络超时，请刷新页面');
		}, 20);
	} else {
		setTimeout(function() {
			const apiType = tools.cache.get('loginType');
			tools.cache.del('app_init');
			if (option.url === '/api/init') {
				tools.alert.message('网络开小差了，请刷新页面', 3000);
				data.errorFun({
					error_code: 'ACCOUNT_ERROR'
				}, 500);
			} else {
				tools.alert.message('网络开小差了，请刷新页面', 3000);
				data.errorFun({
					error_code: 'ACCOUNT_ERROR'
				}, 500);
			}
		}, 20);
	}
}

/* 工具类 */
const tools = {
	api: config.api,
	ajax: function(option) {
		const url = option.url !== undefined ? config.api.url + option.url : '/';
		const data = {
			ajaxData: option.ajaxData || {},
			type: option.type || 'GET',
			isLoading: option.isLoading || false,
			headers: option.headers || null,
			successFun: option.successFun || null,
			errorFun: option.errorFun || null,
			completeFun: option.completeFun || null
		}

		if (data.isLoading) {
			tools.alert.loading();
		}

		let authorization = tools.cache.get('Authorization') || '';

		let headers = {
			'Authorization': authorization
		}

		
		if (data.headers) {
			Object.assign(headers, data.headers);
		}
		
		uni.request({
			url: url,
			data: data.ajaxData,
			method: data.type,
			header: headers,
			timeout: 1000*60,
			success: (res) => {
				//接口请求成功
				if (res.statusCode === 200) {
					if (typeof data.successFun === 'function') {
						try {
							setTimeout(() => {
								data.successFun(res.data);
							}, 20)
						} catch (e) {
							console.error(e);
						}
					}
				} else if (typeof data.errorFun === 'function'){
					try {
						setTimeout(() => {
							data.errorFun(res);
						}, 20)
					} catch (e) {
						console.error(e);
					}
				}
			},
			fail: (error) => {
				ajaxFailFn(error, option, data);
			},
			complete: (e) => {
				//尝试关闭加载动画
				if (data.isLoading) {
					tools.alert.closeLoading();
				}
				if (typeof data.completeFun === 'function') {
					try {
						setTimeout(() => {
							data.completeFun(e);
						}, 200);
					} catch (e) {
						console.error(e);
					}
				}
			}
		});
	},
	getNetworkType: async function() {
		let value = '';
		function next() {
			return new Promise((resolve, reject) => {
				uni.getNetworkType({
					success: function(res) {
						value = res.networkType;
						resolve(res.networkType);
					}
				});
			})
		}
		await next();
		return value;
	},

	uploadFile: function(option) {
		let that = this;
		const url = option.url !== undefined ? config.api.url + option.url : '/';
		const data = {
			filePath: option.filePath || '',
			name: option.name || undefined,
			ajaxData: option.ajaxData || {},
			isLoading: option.isLoading || false,
			successFun: option.successFun || null,
			errorFun: option.errorFun || null,
			completeFun: option.completeFun || null
		}
		let authorization = tools.cache.get('Authorization') || '';
		let headers = {
			'Authorization': authorization,
		}
		if (data.isLoading) {
			tools.alert.loading('上传中...');
		}
		// #ifdef APP-PLUS
		try {
			let videoCompress = uni.requireNativePlugin('Carlos-VideoCompress');
			videoCompress.compress({
				source: plus.io.convertLocalFileSystemURL(data.filePath),
				bitRate: 600000,
				size: 640
			}, result => {
				if (result.status === 'success') {
					next(result.path);
				} else if (result.status !== 'compressing' && result.status !== 'start') {
					next(data.filePath);
				}
			});
		} catch (e) {
			//TODO handle the exception
			console.error('压缩失败')
			next(data.filePath);
		}
		// #endif

		// #ifndef APP-PLUS
		next(data.filePath);
		// #endif

		function next(filePath) {
			uni.uploadFile({
				url: url,
				filePath: filePath,
				fileType: 'image',
				name: data.name || 'file',
				header: headers,
				formData: data.ajaxData,
				success: function(res) {
					if (res.statusCode === 200) {
						if (typeof data.successFun === 'function') {
							try {
								setTimeout(() => {
									data.successFun(JSON.parse(res.data));
								}, 20)
							} catch (e) {
								console.error(e);
							}
						}
					} else {
						setTimeout(() => {
							errorFn(res, option, data);
						}, 20)
					}
				},
				fail: function(error) {
					ajaxFailFn(error, option, data);
				},
				complete: function(e) {
					//尝试关闭加载动画
					tools.alert.closeLoading();

					if (typeof data.completeFun === 'function') {
						try {
							setTimeout(() => {
								data.completeFun(e);
							}, 200);
						} catch (e) {
							console.error(e);
						}
					}
				}
			})
		}
	},
	token: {
		get: function() {
			return tools.cache.get('Authorization');
		},
		set: function(token) {
			return tools.cache.set('Authorization', token);
		}
	},
	cache: {
		get: (key) => {
			try {
				return uni.getStorageSync(key);
			} catch (e) {
				// error
			}
		},
		set: (key, value) => {
			try {
				return uni.setStorageSync(key, value);
			} catch (e) {
				// error
			}
		},
		del: (key) => {
			try {
				uni.removeStorageSync(key);
			} catch (e) {
				// error
			}
		},
		delAll: () => {
			try {
				uni.clearStorageSync();
			} catch (e) {
				// error
			}
		}
	},
	isEmpty: function(value) {
		if (value === null || value === undefined || value === '' || value === false) {
			return true;
		}

		//判断对象
		if (typeof value === 'object' && Object.getOwnPropertyNames(value).length === 0) {
			return true;
		}

		return false;
	},
	/* 删除对象中为空的属性 */
	deleteObj: function(obj) {
		for (var v in obj) {
			if (obj.hasOwnProperty(v)) {
				if (obj[v] === '' || obj[v] === undefined || obj[v] === null) {
					delete obj[v];
				} else if (Array.isArray(obj[v]) && obj[v].length === 0) {
					delete obj[v];
				}
			}
		}
		return obj;
	},
	/* 字符串 */
	string: {
		/* 去掉两边空格 */
		trim: function(str) {
			str = str.toString() || '';
			return str.replace(/(^\s*)|(\s*$)/g, '');
		},

		/* 生成guid */
		getGuid: function() {
			return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
				let r = Math.random() * 16 | 0;
				let v = c == 'x' ? r : (r & 0x3 | 0x8);
				return v.toString(16);
			});
		}
	},
	/* 时间 */
	time: {
		/**
		 * 类似php的日期转时间戳(date)
		 * @param  format    格式
		 * @param  timestamp 时间戳
		 * @return
		 */
		date: function(format, timestamp) {
			if (!timestamp) {
				return '';
			}
			let timeStr = timestamp.toString().length === 10 ? Number(timestamp) * 1000 : timestamp;
			let jsdate = ((timeStr) ? new Date(timeStr) : new Date());
			let pad = function(n, c) {
				if ((n = n + '').length < c) {
					return new Array(++c - n.length).join('0') + n;
				} else {
					return n;
				}
			};
			let txt_weekdays = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
			let txt_ordin = {
				1: 'st',
				2: 'nd',
				3: 'rd',
				21: 'st',
				22: 'nd',
				23: 'rd',
				31: 'st'
			};
			let txt_months = ['', 'January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September',
				'October', 'November', 'December'
			];

			let f = {
				// Day
				d: function() {
					return pad(f.j(), 2);
				},
				D: function() {
					t = f.l();
					return t.substr(0, 3);
				},
				j: function() {
					return jsdate.getDate();
				},
				l: function() {
					return txt_weekdays[f.w()];
				},
				N: function() {
					return f.w() + 1;
				},
				S: function() {
					return txt_ordin[f.j()] ? txt_ordin[f.j()] : 'th';
				},
				w: function() {
					return jsdate.getDay();
				},
				z: function() {
					return (jsdate - new Date(jsdate.getFullYear() + '/1/1')) / 864e5 >> 0;
				},

				// Week
				W: function() {
					let a = f.z(),
						b = 364 + f.L() - a;
					let nd2, nd = (new Date(jsdate.getFullYear() + '/1/1').getDay() || 7) - 1;

					if (b <= 2 && ((jsdate.getDay() || 7) - 1) <= 2 - b) {
						return 1;
					} else {
						if (a <= 2 && nd >= 4 && a >= (6 - nd)) {
							nd2 = new Date(jsdate.getFullYear() - 1 + '/12/31');
							return date('W', Math.round(nd2.getTime() / 1000));
						} else {
							return (1 + (nd <= 3 ? ((a + nd) / 7) : (a - (7 - nd)) / 7) >> 0);
						}
					}
				},

				// Month
				F: function() {
					return txt_months[f.n()];
				},
				m: function() {
					return pad(f.n(), 2);
				},
				M: function() {
					t = f.F();
					return t.substr(0, 3);
				},
				n: function() {
					return jsdate.getMonth() + 1;
				},
				t: function() {
					let n;
					if ((n = jsdate.getMonth() + 1) == 2) {
						return 28 + f.L();
					} else {
						if (n & 1 && n < 8 || !(n & 1) && n > 7) {
							return 31;
						} else {
							return 30;
						}
					}
				},

				// Year
				L: function() {
					let y = f.Y();
					return (!(y & 3) && (y % 1e2 || !(y % 4e2))) ? 1 : 0;
				},
				//o not supported yet
				Y: function() {
					return jsdate.getFullYear();
				},
				y: function() {
					return (jsdate.getFullYear() + '').slice(2);
				},

				// Time
				a: function() {
					return jsdate.getHours() > 11 ? 'pm' : 'am';
				},
				A: function() {
					return f.a().toUpperCase();
				},
				B: function() {
					// peter paul koch:
					let off = (jsdate.getTimezoneOffset() + 60) * 60;
					let theSeconds = (jsdate.getHours() * 3600) +
						(jsdate.getMinutes() * 60) +
						jsdate.getSeconds() + off;
					let beat = Math.floor(theSeconds / 86.4);
					if (beat > 1000) beat -= 1000;
					if (beat < 0) beat += 1000;
					if ((String(beat)).length == 1) beat = '00' + beat;
					if ((String(beat)).length == 2) beat = '0' + beat;
					return beat;
				},
				g: function() {
					return jsdate.getHours() % 12 || 12;
				},
				G: function() {
					return jsdate.getHours();
				},
				h: function() {
					return pad(f.g(), 2);
				},
				H: function() {
					return pad(jsdate.getHours(), 2);
				},
				i: function() {
					return pad(jsdate.getMinutes(), 2);
				},
				s: function() {
					return pad(jsdate.getSeconds(), 2);
				},
				//u not supported yet

				// Timezone
				//e not supported yet
				//I not supported yet
				O: function() {
					let t = pad(Math.abs(jsdate.getTimezoneOffset() / 60 * 100), 4);
					if (jsdate.getTimezoneOffset() > 0) t = '-' + t;
					else t = '+' + t;
					return t;
				},
				P: function() {
					let O = f.O();
					return (O.substr(0, 3) + ':' + O.substr(3, 2));
				},
				//T not supported yet
				//Z not supported yet

				// Full Date/Time
				c: function() {
					return f.Y() + '-' + f.m() + '-' + f.d() + 'T' + f.h() + ':' + f.i() + ':' + f.s() + f.P();
				},
				//r not supported yet
				U: function() {
					return Math.round(jsdate.getTime() / 1000);
				}
			};

			return format.replace(/[\\]?([a-zA-Z])/g, function(t, s) {
				let ret;
				if (t != s) {
					// escaped
					ret = s;
				} else if (f[s]) {
					// a date function exists
					ret = f[s]();
				} else {
					// nothing special
					ret = s;
				}
				return ret;
			});
		},

		/**
		 * 类似php的日期转时间戳(strtotime)
		 * @param  datetime
		 * @return
		 */
		strtotime: function(datetime) {
			let p = {};
			p.datetime = datetime || '';
			datetime = undefined;
			p._ = p.datetime.toString().indexOf(' ') == -1 ? (p.datetime.toString().indexOf(':') == -1 ? [p.datetime, ''] : [
				'', p.datetime
			]) : p.datetime.split(' ');
			p.ymd = p._[0] || '';
			p.his = p._[1] || '';
			p.ymd = p.ymd.toString().indexOf('-') == -1 ? [p.ymd] : p.ymd.split('-');
			p.his = p.his.toString().indexOf(':') == -1 ? [p.his] : p.his.split(':');
			p.year = (p.ymd[0] || 0) - 0;
			p.month = (p.ymd[1] || 0) - 1;
			p.day = (p.ymd[2] || 0) - 0;
			p.hour = p.his[0] - 0;
			p.minute = p.his[1] - 0;
			p.second = p.his[2] - 0;
			//兼容无"时:分:秒"模式
			if ((p.his[0] == undefined) || isNaN(p.hour)) {
				p.hour = 0;
			}
			if ((p.his[1] == undefined) || isNaN(p.minute)) {
				p.minute = 0;
			}
			if ((p.his[2] == undefined) || isNaN(p.second)) {
				p.second = 0;
			}
			p.time = (new Date(p.year, p.month, p.day, p.hour, p.minute, p.second)).getTime();
			p = parseInt(p.time);
			// p = parseInt(p.time / 1000);
			return p;
		},

		/* 获取当前时间-毫秒时间戳*/
		now: function() {
			return (new Date()).getTime();
		},

		/* 获取当前时间-秒时间戳 */
		timestamp: function() {
			return parseInt(this.now() / 1000);
		}
	},
	url: {
		//获取浏览器地址栏参数并转化为对象
		getSearchParams: function(params){
			if(params){
				return Object.fromEntries(new URLSearchParams(params));
			}else{
				return Object.fromEntries(new URLSearchParams(window.location.search.substr(1)));
			}
		},

		/* 替换地址栏中的参数 */
		replaceParamVals: function(params, url) {
			let tempArr = [];
			if(typeof params === 'object'){
				for(let [key,value] of Object.entries(params)){
					tempArr.push(key+'='+value);
				}
				return url + '?' + tempArr.join('&');
			}else{
				return url;
			}
		},

		isJump: false,

		/* 跳转 */
		toUrl: function(url, data) {
			if (tools.url.isJump) {
				return;
			}
			tools.url.isJump = true;
			let _url = tools.url.replaceParamVals(data, url);
			uni.navigateTo({
				url: _url
			});
			setTimeout(() => {
				tools.url.isJump = false;
			}, 200)
		},

		// 关闭所有页面，打开到应用内的某个页面
		reLaunch: function(url, data) {
			if (tools.url.isJump) {
				return;
			}
			tools.url.isJump = true;
			let _url = tools.url.replaceParamVals(data, url);
			uni.reLaunch({
				url: _url
			})
			setTimeout(() => {
				tools.url.isJump = false;
			}, 200)
		},

		// 跳转到 tabBar 页面，并关闭其他所有非 tabBar 页面
		switchTab: function(url, data) {
			if (tools.url.isJump) {
				return;
			}
			tools.url.isJump = true;
			let _url = tools.url.replaceParamVals(data, url);
			uni.switchTab({
				url: _url
			});
			setTimeout(() => {
				tools.url.isJump = false;
			}, 200)
		},

		/* 返回 */
		back: function(num) {
			if (tools.url.isJump) {
				return;
			}
			tools.url.isJump = true;
			uni.navigateBack({
				delta: num || 1
			});
			setTimeout(() => {
				tools.url.isJump = false;
			}, 200)
		},

		/* 关闭当前页面，跳转到应用内的某个页面 */
		toReplace: function(url, data) {
			if (tools.url.isJump) {
				return;
			}
			tools.url.isJump = true;
			let _url = tools.url.replaceParamVals(data, url);
			uni.redirectTo({
				url: _url
			})
			setTimeout(() => {
				tools.url.isJump = false;
			}, 200)
		}
	},
	mywx:{
		wx: jweixin,
		wxShare:function(obj) {
			window.wx = jweixin;
			let data = {
				api_url: '/api/wechat/js/signatures',/* 汉子接口 */
				ajax_data: {
					url:location.host + location.pathname
				},
				api_type: 'POST',
				api_list: [],
				hide_menu_items: [],
				show_menu_items: [],
				debug: false,
				success: obj.success || ''
			};
			Object.assign(data, obj);
			if (obj.hideAllNonBaseMenuItem) {
				data.api_list.push('hideAllNonBaseMenuItem');
			}

			data.api_list.push('hideMenuItems');
			data.api_list.push('showMenuItems');

			//默认为分享朋友圈和好友
			data.api_list.push('onMenuShareTimeline');
			data.api_list.push('onMenuShareAppMessage');

			// 选择图片上传图片
			data.api_list.push('chooseImage');
			data.api_list.push('uploadImage');

			// 关闭网页窗口
			data.api_list.push('closeWindow');

			tools.ajax({
				url: data.api_url,
				ajaxData: data.ajax_data,
				type: data.api_type,
				successFun: function(res) {
					//全局配置
					let config = {
						debug: data.debug,
						appId: res.data.sign.appId,
						timestamp: res.data.sign.timestamp,
						nonceStr: res.data.sign.nonceStr,
						signature: res.data.sign.signature,
						jsApiList: data.api_list
					};
					if (data.success) {
						data.success(data);
					}
					wx.config(config);

					wx.ready(function() {
						// 分享到朋友圈
						wx.onMenuShareTimeline({
							title: data.title, // 分享标题
							link: data.url, // 分享链接
							imgUrl: data.img, // 分享图标
							success: function() {
								// 用户确认分享后执行的回调函数
								if (obj.isLimitBuy) {
									let r_url = location.href;
									location.replace(r_url);
								}
							},
							cancel: function() {
								// 用户取消分享后执行的回调函数
							}
						});

						// 分享给朋友
						wx.onMenuShareAppMessage({
							title: data.title, // 分享标题
							desc: data.desc, // 分享描述
							link: data.url, // 分享链接
							imgUrl: data.img, // 分享图标
							type: 'link', // 分享类型,music、video或link，不填默认为link
							dataUrl: '', // 如果type是music或video，则要提供数据链接，默认为空
							success: function() {
								// 用户确认分享后执行的回调函数
								if (obj.isLimitBuy) {
									let r_url = location.href;
									location.replace(r_url);
								}
							},
							cancel: function() {
								// 用户取消分享后执行的回调函数
							}
						});

						if (obj.hideAllNonBaseMenuItem) {
							wx.hideAllNonBaseMenuItem(); // 隐藏所有非基础按钮
						}

						wx.hideMenuItems({
							menuList: data.hide_menu_items // 要隐藏的菜单项，只能隐藏“传播类”和“保护类”按钮
						});

						wx.showMenuItems({
							menuList: data.show_menu_items // 要显示的菜单项
						});
					});
				}
			});
		}
	}
}
export default tools;
