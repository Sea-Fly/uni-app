import errorHnadle from './errorHandle';

let timeInter;
export default {
    object: {
        path: 'ws://127.0.0.1',
        onMessage: '',
        onClose: '',
        onError: '',
        onOpen: '',
        tokenUrl: '/',
        token: '',
        loginPath: 'login',
        socket: {},

        /* 修正内容格式 */
        filterContent: function(path, data) {
            let message = {
                path: path,
                data: data
            };

            return JSON.stringify(message);
        },

        /* 发送消息 */
        sendMessage: function(path, data) {
            let result = this.filterContent(path, data);
            return uni.sendSocketMessage({
				data: result
			});
        }
    },

    /* 初始化 */
    init: function(obj) {
        let that = this;

        that.object.path = obj.path || 'ws://127.0.0.1'; //wesocket的地址
        that.object.loginPath = obj.loginPath || 'login'; //wesocket的地址
        that.object.onMessage = obj.onMessage || ''; //收到信息时的回调函数
        that.object.onClose = obj.onClose || 'ws://127.0.0.1'; //连接关闭时回调函数
        that.object.onError = obj.onError || 'ws://127.0.0.1'; //连接失败时回调函数
        that.object.onOpen = obj.onOpen || 'ws://127.0.0.1'; //连接成功时回调函数
        that.object.tokenUrl = obj.tokenUrl || 'ws://127.0.0.1'; //登录token接口地址
        that.noReconnect = false;
        //获取登录token
        that.getToken();
    },

    /* 连接socket */
    connect: function() {
        let that = this;
        let socket = uni.connectSocket({
			url: that.object.path,
			complete: ()=> {}
		});
		
        that.object.socket = socket;
        let object = that.object;

        //监听Socket的错误
        socket.onerror = function(event) {
            clearInterval(timeInter);
            if (typeof object.onError == 'function') {
                object.onError(event);
            }
        };
		uni.onSocketError(socket.onerror);

        // 监听Socket的关闭
        socket.onclose = function (event) {
			clearInterval(timeInter);
			if (typeof object.onClose == 'function') {
			    object.onClose(event);
			}
			if (event.code !== 1000 && !that.noReconnect) { // 表示没有收到预期的状态码
			    that.reconnectTime = setTimeout(() => {
			        that.getToken();
			    }, 2000);
			}
		};
		uni.onSocketClose(socket.onclose);

        // 监听Socket的收到信息
        socket.onmessage = function(event) {

            let message = JSON.parse(event.data);

            if (message.cmd === 'logined') { //收到成功登录提示
                //登录
                console.debug('Login Socket Success!');
                timeInter = setInterval(() => {
                    that.sendMessage('ping');
                }, 30000);
            } else if (message.cmd === 'error' && message.data) {
                //先执行全局的错误检测,在执行自定义方法
                let errcode = message.data.code;
                if (typeof errorHnadle[errcode] == 'function') {
                    try {
                        errorHnadle[errcode](message.data);
                    } catch (e) {
                        console.error(e);
                    }
                } else {
                    uni.tools.alert.error(message.data.message);
                }
            }

            if (typeof object.onMessage == 'function') {
                object.onMessage(event, message);
            }
        };
		uni.onSocketMessage(socket.onmessage)
        //监听Socket的打开
        socket.onopen = function(event) {

            //发送token
            object.sendMessage(object.loginPath, { token: object.token });

            if (typeof object.onOpen == 'function') {
                object.onOpen(event);
            }
        };
		uni.onSocketOpen(socket.onopen)

        return socket;
    },

    /* 关闭连接 */
    close: function() {
        let that = this;
        that.noReconnect = true;
        clearTimeout(that.reconnectTime);
		uni.closeSocket();
		that.object.socket = {};
    },

    /* 发送消息 */
    sendMessage: function(path, data) {
        return this.object.sendMessage(path, data);
    },

    /* 获取token */
    getToken: function() {
        let that = this;
        uni.tools.ajax({
            url: that.object.tokenUrl,
            ajaxData: {},
            successFun: function(res) {
                that.object.token = res.data.token;
                that.connect();
            },
            errorFun: function(error_data) {
                if (error_data.http_code === 404) {
                    return;
                }
                uni.tools.alert.error('socket登录失败,尝试重新登录中..');
                // setTimeout(function() {
                //     that.getToken();
                // }, 5000);
            },
            type: 'POST'
        });
    }
};
