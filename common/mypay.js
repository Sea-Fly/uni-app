import Tools from './tools.js';
// import store from '@/store';

var device = "wap";

// #ifdef APP-PLUS
device = "app";
plus.payment.getChannels(function(channels){
	wechatpay = channels.find(el => el.id == 'wxpay');
	alipay = channels.find(el => el.id == 'alipay');
},function(e){  
	Tools.alert.error("获取支付通道失败：" + e.message);  
}); 
// #endif

// #ifdef H5
device = "wap";
// #endif


// app 微信支付
function appWxpay(obj){
	return new Promise((resolve, reject)=>{
		uni.requestPayment({
		    provider: 'wxpay',
		    orderInfo: obj, //微信、支付宝订单数据
		    success: function (res) {
				console.log(res);
		        resolve(res);
		    },
		    fail: function (err) {
		        console.log(err);
				reject(err);
		    }
		});
	})
	// 第二种方法
	// return new Promise((resolve, reject)=>{
	// 	plus.payment.request(wechatpay, obj, function(res){
	// 		resolve(res);
	// 	}, function(error){
	// 		reject(error)
	// 	})
	// })
}

// app 支付宝支付
function appAlipay(obj){
	return new Promise((resolve, reject)=>{
		uni.requestPayment({
		    provider: 'alipay',
		    orderInfo: obj.sign, //微信、支付宝订单数据
		    success: function (res) {
		        resolve(res);
		    },
		    fail: function (err) {
				reject(err);
		    }
		});
	})
}

//h5 微信支付
function h5Wxpay(obj){
	// debugger;
	return new Promise((resolve, reject)=>{
		if(WeixinJSBridge && WeixinJSBridge.invoke){
			WeixinJSBridge.invoke(
				'getBrandWCPayRequest',
				{
					"appid":obj.appId || obj.appid,
					"appId":obj.appId || obj.appid,           //公众号名称，由商户传入
					"timeStamp":obj.timeStamp,   //时间戳，自1970年以来的秒数
					"nonceStr":obj.nonceStr,     //随机串
					"package":obj.package,
					"signType":obj.signType,     //微信签名方式：
					"paySign":obj.paySign,		 //微信签名
				},function(res){
					if (res.err_msg === "get_brand_wcpay_request:ok") {
						console.log('支付成功');
						resolve(res);
						return
					} else if (res.err_msg === "get_brand_wcpay_request:cancel") {
						console.log('取消支付');
						reject('取消支付');
						return
					} else if (res.err_msg === "get_brand_wcpay_request:fail") {
						console.log('支付失败');
						Tools.alert.error(error_msg);
						reject('支付失败');
						return
					}
					reject(res.errMsg);
				},function(err){
					reject(err);
				});
		}
	})
}

export default {
	// 获取支付信息
	getPaydata: function(ajaxData, url){
		return new Promise((resolve, reject) => {
			Tools.ajax({
				url: url || '/api/order/pays',
				type: 'POST',
				ajaxData: ajaxData,
				successFun: function(res){
					console.log(res.data)
					if(res.data){
						var obj = {
							...res.data,
							"appid": res.data.appid,
							"nonceStr": res.data.noncestr,
							"package": res.data.package,
							"partnerid": res.data.partnerid,
							"prepayid": res.data.prepayid,
							"timeStamp": res.data.timestamp,
							"sign": res.data.sign,
							"paySign": res.data.sign,
						}
						// 由于历史遗留问题，后端返回的字段不一致
						if(device == "wap" && url){
							obj.appId = res.data.appId;
							obj.timeStamp = res.data.timeStamp;
							obj.nonceStr = res.data.nonceStr;
							obj.paySign = res.data.paySign;
						}
						if(ajaxData.device == 'mp'){
							obj = res.data.sign;
						}
						resolve(obj);
					}else{
						Tools.alert.error('支付失败，网络错误！')
					}
				},
				errorFun: function(errorData, status, headers, errorObj) {
					reject(errorData);
					console.log(errorData)
				}
			})
		})
	},
	
	pay: function(ajaxData, url){
		return new Promise((resolve, reject) => {
			// const status = store.state.status;
			const status = {is_auth:0};
			if(device == 'app'){
				ajaxData.device = 'APP';
			}else{
				ajaxData.device = 'mp';
			}
			if(!status.is_auth && ajaxData.device === 'mp'){
				// h5微信支付，判断是否已经授权
				reject({ msg: '未绑定微信！请先绑定微信！' });
			}
			this.getPaydata(ajaxData, url).then((obj) => {
				if(ajaxData.type == 'wechatpay'){
					if(device == "app"){
						appWxpay(obj).then(resolve).catch(reject);
					}else{
						h5Wxpay(obj).then(resolve).catch(reject);
					}
					return;
				}
				if(ajaxData.type == 'alipay'){
					if(device == 'app'){
						appAlipay(obj).then(resolve).catch(reject);
					}else{
						reject({ msg: '暂无支付宝支付' });
						//Tools.alert.error('H5暂无支付宝支付');
					}
					return
				}
				reject({ msg: '暂无支付宝支付' });
				//Tools.alert.error('H5暂无支付宝支付');
			})
		})
	},
}