export default{
    data(){
        return {
            //设置默认的分享参数
            share:{
                title:'小程序分享，快来看看吧！',
                path:'/pages/index/index',
                imageUrl:'https://wx.yizhangxiao.com/image/data/yzx/file/image/upload/project/work-active.png',
            }
        }
    },
    onShareAppMessage(res) {
        return {
            title:this.share.title,
            path:this.share.path,
            imageUrl:this.share.imageUrl,
            success(res){
                // uni.showToast({
                //     title:'分享成功'
                // })
            },
            fail(res){
                // uni.showToast({
                //     title:'分享失败',
                //     icon:'none'
                // })
            }
        }
    }
}