export default {
	loading: (title) => {
		uni.showLoading({
			title: title || '加载中...',
			mask: true
		})
	},
	closeLoading: () => {
		uni.hideLoading();
	},
	info: (obj, sucFun) => {
		return new Promise((resolve, reject) => {
			let content = obj,
				title = '提示',
				confirmText = '确定';
			if (typeof obj === 'object') {
				title = obj.title || '提示';
				content = obj.content;
				confirmText = obj.confirmText || '确定';
			}
			// #ifdef APP-PLUS
			let data = { title, content, confirmText, type: 'info' };
			uni.$once('commonAlert', function(type){
				if (type === 'confirm') {
					typeof sucFun === 'function' ? sucFun() : null;
					resolve();
				} else {
					typeof failFun === 'function' ? failFun() : null;
					reject();
				}
			});
			uni.tools.url.toUrl('/pages/common/alert', data);
			// #endif
			// #ifndef APP-PLUS
			uni.showModal({
				title,
				content,
				confirmText,
				showCancel: false,
				success: function(res) {
					if (res.confirm) {
						typeof sucFun === 'function' ? sucFun() : null;
						resolve();
					}
				}
			})
			// #endif
		})
	},
	/* 确认输入框 */
	confirm: function(obj, sucFun, failFun) {
		return new Promise((resolve, reject) => {
			let content = obj,
				title = '提示',
				confirmText = '确定';
			if (typeof obj === 'object') {
				title = obj.title || '提示';
				content = obj.content;
				confirmText = obj.confirmText || '确定';
			}
			// #ifdef APP-PLUS
			let data = { title, content, confirmText, type: 'confirm' };
			uni.$once('commonAlert', function(type){
				if (type === 'confirm') {
					typeof sucFun === 'function' ? sucFun() : null;
					resolve();
				} else {
					typeof failFun === 'function' ? failFun() : null;
					reject();
				}
			});
			uni.tools.url.toUrl('/pages/common/alert', data);
			// #endif
			// #ifndef APP-PLUS
			uni.showModal({
				title,
				content,
				confirmText,
				success: function(res) {
					if (res.confirm) {
						typeof sucFun === 'function' ? sucFun() : null;
						resolve();
					} else if (res.cancel) {
						typeof failFun === 'function' ? failFun() : null;
						reject();
					}
				}
			})
			// #endif
		})
	},
	/* 消息提示 */
	message: function(msg, duration) {
		duration = duration !== undefined ? duration : 3000
		uni.showToast({
			title: msg,
			// mask: true,
			icon: 'none',
			duration: duration
		});
	},
	
	warning: function(msg, duration) {
		duration = duration !== undefined ? duration : 3000
		uni.showToast({
			title: msg,
			// mask: true,
			icon: 'none',
			duration: duration
		});
	},
	
	success: function(msg, duration) {
		duration = duration !== undefined ? duration : 3000
		uni.showToast({
			title: msg,
			// mask: true,
			icon: 'success',
			duration: duration
		})
	},
	error: function(msg, duration) {
		duration = duration !== undefined ? duration : 3000
		uni.showToast({
			title: msg,
			// mask: true,
			icon: 'none',
			// image: '../../static/images/login/phone.png',
			duration: duration
		})
	}
}
