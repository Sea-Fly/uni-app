// import store from '@/store';

let is_init = true,
	array = [],
	num = 0;

function resetFn() {
	is_init = true;
	array = [];
	num = 0;
}
function getCurPage(){
    let pages = getCurrentPages();
    let curPage = pages[pages.length-1];
    return curPage
}

function next(dataArr) {
	for (let d of dataArr) {
		num++;
		uni.tools.ajax(d);
	}
	if (num === dataArr.length) {
		resetFn();
	}
}

function fn(array) {
	uni.session.status.is_user = 0;
	delete uni.session.status.type;
	delete uni.session.status.accountType;
	// store.commit('GET_STATUS', uni.session.status);
	let curPage = getCurPage();
	if (curPage.route === 'pages/login/login') {
		next(array);
	} else if (curPage.route === 'pages/index/index') {
		for (let d of array) {
			if (d.url === '/api/jobs/company/status') {
				uni.tools.ajax(d);
			}
		}
		// next(array);
	} else {
		resetFn();
		const page = getCurrentPages();
		const callback = page[page.length-1] ?
			encodeURIComponent(uni.tools.url.replaceParamVals(page[page.length-1].options || {}, '/' + page[page.length-1].route)) :
		uni.tools.alert.confirm('未登录，前往登录', ()=> {
			// uni.tools.url.toUrl('/pages/login/login',{callback:callback});
			uni.tools.url.toUrl('/pages/index/index',{callback:callback});
		}, ()=> {
			uni.tools.url.switchTab('/pages/index/index');
		});
	}
}	
	
export default {
	//非法token    
	'AUTHORIZATION_INVALID': function(tools, option) {
		array.push(option);
		if (is_init) {
			is_init = false;
			tools.token.init().then(() => {
				fn(array);
			})
		}
		return false;
	},
	'NO LOGIN': function(tools, option) {
		array.push(option);
		if (is_init) {
			is_init = false;
			tools.token.init().then(() => {
				fn(array);
			})
		}
		return false;
	},
	'ACCOUNT_ERROR': function(tools, option, error_data) {
		array.push(option);
		let arr = ['/api/jobs/company/status'];
		if (is_init) {
			is_init = false;
			return new Promise((resolve, reject)=>{
				uni.tools.alert.info(error_data.error_msg, ()=>{
					tools.token.init().then(() => {
						fn(array);
						resolve(arr.indexOf(option.url) !== -1);
					})
				});
			})
		}
		return false;
	}
};
