import Vue from 'vue';
import tools from '@/common/tools.js';
/*
    **混合 （请认真查看使用方法 https://vuefe.cn/v2/guide/mixins.html）
    **注意同名钩子函数将混合为一个数组，因此都将被调用。且先调用混合对象的钩子
    **
*/
const imgApi = 'https://wx.yizhangxiao.com/image/data/yzx/file/image/upload/project/'; //oss图片域名
const logoApi = 'https://wx.yizhangxiao.com/image';
Vue.mixin({
	// #ifdef MP-WEIXIN
	// onShareAppMessage(res) {
	// 	return {
	// 		title: '',
	// 		path: '/pages/index/index',
	// 		imageUrl: `${imgApi}static/images/share.jpeg`
	// 	}
	// },
	// #endif
	computed: {
		logoApi: function() {
			return logoApi;
		},
		imgApi: function() {
			return imgApi;
		},
		defaultHeadimg: function() {
			return this.imgApi + 'jg-logo.png';
		},
        //是否IPhoneX
		isIPhoneXS: function(){
			let isXS = false;
			uni.getSystemInfo({
				success(res) {
					// console.log('手机牌子：'+res.brand) //手机牌子
					// console.log('手机型号'+res.model) //手机型号
					// console.log(res.screenWidth) //屏幕宽度
					// console.log(res.screenHeight) //屏幕高度
					// console.log(res.model.indexOf('iPhone XS'));
					if(res.model.indexOf('iPhone X')!=-1){
						isXS = true;
					}else{
						isXS = false;
					}
				}
			});
			return isXS;
		},
	},
    /* 全局过滤 */
    filters: {
        /* 默认图片过滤 */
        thumbnail: function(val, size) {
            let img_size = '?x-oss-process=image/resize,w_' + size;
            return !val ? app.defaultImg() : val + (!size ? '' : img_size);
        },
        msgChatTime: function(d) {
            let that = this;
            let dateT = new Date(d), dateNow = new Date();
            // 当天
            let n_year = dateNow.getFullYear(),
                n_month = dateNow.getMonth() + 1,
                n_date = dateNow.getDate();
            //当天0时0分0秒的时间戳
            dateNow.setHours(0);
            dateNow.setMinutes(0);
            dateNow.setSeconds(0);
            let datetime0 = (new Date(dateNow)).getTime();

            let hour = dateT.getHours(),
                prefixTxt = hour >= 12 ? '下午' : '上午',
                min = dateT.getMinutes(),
                year = dateT.getFullYear(),
                month = dateT.getMonth() + 1,
                date = dateT.getDate(),
                datetime = dateT.getTime();
            // 同一天，显示上午下午
            if (datetime - datetime0 >= 0 && datetime - datetime0 < 3600 * 24 * 1000) {
                return prefixTxt + (hour > 12 ? hour - 12 : hour) + ':' + (min >= 10 ? min : 0 + '' + min);
            } else if (datetime0 - datetime > 0 && datetime0 - datetime <= 3600 * 24 * 1000) {
                return '昨天' + ' ' + prefixTxt + (hour > 12 ? hour - 12 : hour) + ':' + (min >= 10 ? min : 0 + '' + min);
            } else if (datetime0 - datetime > 3600 * 24 * 1000 * 2 && datetime0 - datetime <= 3600 * 24 * 1000 * 6) {
                let idx = dateT.getDay(), txt = '';
                switch (idx) {
                case 0: txt = '星期日'; break;
                case 1: txt = '星期一'; break;
                case 2: txt = '星期二'; break;
                case 3: txt = '星期三'; break;
                case 4: txt = '星期四'; break;
                case 5: txt = '星期五'; break;
                case 6: txt = '星期六'; break;
                }
                return txt + ' ' + prefixTxt + (hour > 12 ? hour - 12 : hour) + ':' + (min >= 10 ? min : 0 + '' + min);
            }
            //  一周外，yyyy/m/d
            return year + '年' + month + '月' + date + '日 ' + prefixTxt + (hour > 12 ? hour - 12 : hour) + ':' + (min >= 10 ? min : 0 + '' + min);
        },
		/* 时间转换 */
        timeTran: function(value) {
            return !parseInt(value) ? '-' : tools.time.date('Y-m-d H:i:s', value);
        },
        timeTrans: function(value) {
            return tools.time.date('Y-m-d H:i:s', value);
        },
        timeTranYMD:function(value){
            return tools.time.date('Y-m-d', new Date(value).getTime());
        },
        /* 自定时间格式转换 */
        setTimeTran: function(value, type) {
            return value ? parseInt(value) === 0 ? '-' : tools.time.date(type, value) : '-';
        },

        /* 价格转换--保留两位小数 */
        toFixedPrice: function(value) {
			if (!value) return '0';
            return value ? (parseInt(value) / 100).toFixed(2) : '0';
        },
		// 小数转化
		decimal:function(value){
			// console.log('valuedd: ', value);
			if (!value) return '.00';
			let text = value ? (parseInt(value) / 100).toFixed(2) : '.00';
			return text.substring(text.indexOf("."),text.indexOf(".")+3);
		},
		// 折扣小数转化
		discountDecimal:function(value){
			if (!value) return '';
			let text = value ? parseInt(value).toFixed(2) : '';
			return text.indexOf('.00') != -1 ? '' : text.substring(text.indexOf("."),text.indexOf(".")+3);
		},
        /* 价格转换 */
        toPrice: function(value) {
			// console.log('value: ', value);
            return value ? (parseInt(value / 100)) : '0';
        },

        /* 时间转换 */
        dayTimeTran: function(value) {
            let dayTime = new Date().getTime();
            let t = parseInt((dayTime - value) / 1000);

            if (t > 3600) {
                return parseInt(value) === 0 ? '-' : tools.time.date('Y-m-d H:i:s', value);
            } else if (t < 60) {
                return '刚刚';
            } else {
                return parseInt(t / 60) + '分钟前';
            }
        },
        formatSeconds(value) {
            var theTime = parseInt(value);
            var theTime1 = 0;
            var theTime2 = 0;
            var theTime3 = 0;
            if (theTime > 60) {
                theTime1 = parseInt(theTime / 60);
                theTime = parseInt(theTime % 60);
                if (theTime1 > 60) {
                    theTime2 = parseInt(theTime1 / 60);
                    theTime1 = parseInt(theTime1 % 60);
                    if (theTime2 > 24) {
                        theTime3 = parseInt(theTime2 / 24);
                        theTime2 = parseInt(theTime2 % 24);
                    }
                }
            }
            var result = '';
            if (theTime > 0) {
                result = '' + parseInt(theTime) + '秒';
            }
            if (theTime1 > 0) {
                result = '' + parseInt(theTime1) + '分' + result;
            }
            if (theTime2 > 0) {
                result = '' + parseInt(theTime2) + '小时' + result;
            }
            if (theTime3 > 0) {
                result = '' + parseInt(theTime3) + '天' + result;
            }
            return result;
        },
        /* 时长 */
        durationTran: function(value) {
            let val = '';

            if (value == 0) {
                return '-';
            } else if (value < 60) {
                return value + '秒';
            } else {
                let m = parseInt(val / 60);
                return `${m}分钟`;
            }
        },

        priceFormat: function(val) { //val是以分为单位
            if (typeof val === 'undefined') {
                return;
            }
            if (val == 0) {
                return '0';
            }
            var val_str = '',
                val_arr = [];
            val_str = val.toString();
            val_arr = val_str.split('');
            //判断价格位数
            if (val_arr.length <= 5) {
                return (val / 100);
            } else {
                val_arr.splice(val_arr.length - 2); //添加小数点
                for (var i = val_arr.length, num = 0; i > 0; i--, num++) {
                    if (num == 3) {
                        val_arr.splice(i, 0, ','); //添加逗号
                        num = 1;
                        i--;
                    }
                }
                return val_arr.join('');
            }
        },
        fixedPriceFormat: function(val) { //val是以分为单位
            if (typeof val === 'undefined') {
                return;
            }
            if (val == 0) {
                return '0';
            }
            var val_str = '',
                val_arr = [];
            val_str = val.toString();
            val_arr = val_str.split('');
            //判断价格位数
            if (val_arr.length <= 5) {
                return (val / 100).toFixed(2);
            } else {
                val_arr.splice(val_arr.length - 2, 0, '.'); //添加小数点
                for (var i = val_arr.length - 4, num = 1; i > 0; i--, num++) {
                    if (num == 3) {
                        val_arr.splice(i, 0, ','); //添加逗号
                        num = 1;
                        i--;
                    }
                }
                return val_arr.join('');
            }
        },
        //价格转换(保留两位小数,不足补零,大数字3位数加一个逗号)
        priceFormats: function(val) { //val是以分为单位
            if (typeof val === 'undefined') {
                return;
            }
            if (val == 0) {
                return '0';
            }
            var val_str = '',
                val_arr = [];
            val_str = val.toString();
            val_arr = val_str.split('');
            //判断价格位数
            if (val_arr.length <= 5) {
                return (val / 100).toFixed(2);
            } else {
                let val_str1 = val_str.split('-');
                if (val_str1[0] && val_str1[1]) {
                    val_str1 = ((val_str1[0]) / 100).toFixed(2) + '-' + ((val_str1[1]) / 100).toFixed(2);
                } else {
                    val_str1 = (val_str1[0] / 100).toFixed(2);
                }
                return val_str1;
            }
        },

        /* 手机号码隐私处理 */
        setPhone: function(val) {
            if (val == null || val == undefined || !val) {
                return '-';
            }
            let reg = /(\d{3})\d*(\d{4})/,
                val_sub = val.toString();
				
            return val_sub.replace(reg, '$1****$2');
        },

        /* 身份证号码隐私处理 */
        setIdCard:function(val){
            return val.replace(/^(\d{6})\d*(\d{4})$/,'$1********$2') || '';
        },

        /* 数字千分位转换 */
        format: function(value) {
            let reg = /\d{1,3}(?=(\d{3})+$)/g;
            return (value + '').replace(reg, '$&,');
        },
		
		/* 视频时长转换 */
		videoDurationFilter: function(value) {
			if (!value) return '0″';
			let time = Number(value);
			if (time < 60) return `${time}″`;
			let min = parseInt(time / 60);
			let s = time % 60;
			return `${min}′${s}″`;
		},
		/* 阿拉伯数字转中文数字 */
		numFormat: function(v) {
			if (v === undefined || v === null || isNaN(v)) return;
			let arr = new Array("零", "一", "二", "三", "四", "五", "六", "七", "八", "九");
			return arr[Number(v)];
        },

        /* decode解码 */
        toDecodeString:function(value){
            return decodeURIComponent(value);
        }
    },
    methods: {
		
        /* 跳转 */
        toUrl: tools.url.toUrl,
		
		// 关闭所有页面，打开到应用内的某个页面
		reLaunch: tools.url.reLaunch,
		
		// 跳转到 tabBar 页面，并关闭其他所有非 tabBar 页面
		switchTab: tools.url.switchTab,

        /* 返回 */
        back: tools.url.back,

        /* 关闭当前页面，跳转到应用内的某个页面 */
        toReplace: tools.url.toReplace,
		/* 时间转换 */
		timeTran: function(value) {
			return !parseInt(value) ? '-' : tools.time.date('Y-m-d H:i:s', value);
		},
		/* 自定时间格式转换 */
		setTimeTran: function(value, type) {
		    return value ? parseInt(value) === 0 ? '-' : tools.time.date(type, value) : '-';
		},
		// 预览图片
		seeImg(url, urls) {
			uni.previewImage({
				current: url,
				urls: urls || [url]
			})
        },
        
        //设置小程序标题
        setTitle(title){
            /* 设置标题 */
            uni.setNavigationBarTitle({
                title:title
            });
        },

        //判断所属平台,默认判断是否android平台.
        isPlatform(value){
            if(value){
                return uni.getSystemInfoSync().platform == value;
            }else{
                return uni.getSystemInfoSync().platform == 'android';
            }
        },

        /* 小程序复制 */
        doCopy(copy_text) {
            // 在微信小程序中
            // #ifdef MP-WEIXIN
                uni.setClipboardData({
                    data: copy_text,
                    success: function () {
                        uni.tools.alert.message('已复制到剪切板');
                    }
                });
            // #endif
        },

        /* 预览图片
         currentImg：当前图片路径
         arrImg：预览多张图片所传数组
         imgKey：如果不是普通的图片数组，则需要传图片路径所对应的键名
         */
        previewImage(currentImg,arrImg,imgKey) {
            let arrTemp = [];
            if(imgKey){
                arrImg.forEach(item=>{
                    arrTemp.push(item[imgKey]);
                })
            }
            /* 预览单张图片 */
            if(!arrImg){
                arrImg = [currentImg];
            }
            //预览图片
            uni.previewImage({
                urls: arrTemp.length==0?arrImg:arrTemp,
                current: currentImg
            });
        },

        // 小程序分享
		uniShowShare(obj) {
			let that = this,
				o = obj || {};
			let title = o.title || '测试',
				path = o.path || 'pages/index/index',
				imageUrl = o.imageUrl || '/static/images/logo.png',
				summary = o.summary || '测试';
			uni.showShareMenu({
				path: path,
				imageUrl: imageUrl,
				title: title,
				content: summary,
				success: function(res) {
					console.log("success:" + JSON.stringify(res));
				},
				fail: function(err) {
					console.log("success:" + JSON.stringify(err));
				}
			});
		},
	
		developFn(txt) {
			uni.tools.alert.info(txt?txt:'开发中~');
		},
		
		// 呼叫手机
		phoneCall(phone) {
			if (!phone || phone === '-') {
				return
			}
			uni.makePhoneCall({
				phoneNumber: phone.toString() //仅为示例
			});
		},

        //一次性订阅消息
		message_subscribe(arr){
			uni.requestSubscribeMessage({
				tmplIds: arr,//消息模板数组，最多只能填写三个模板ID
				success (res) {
					console.log('success');
					// console.log(res);
				 },
				fail (res) {
					console.log('fail');
				 }
			})
		},

         /**
         * 坐标转换，百度地图坐标转换成腾讯地图坐标
         * lng 腾讯经度（pointy）
         * lat 腾讯纬度（pointx）
         * 经度>纬度
         */
        bMapToQQMap(lng, lat) {
            if (lng == null || lng == '' || lat == null || lat == '') return [lng, lat]
            var x_pi = 3.14159265358979324
            var x = parseFloat(lng) - 0.0065
            var y = parseFloat(lat) - 0.006
            var z = Math.sqrt(x * x + y * y) - 0.00002 * Math.sin(y * x_pi)
            var theta = Math.atan2(y, x) - 0.000003 * Math.cos(x * x_pi)
            var lng = (z * Math.cos(theta)).toFixed(7)
            var lat = (z * Math.sin(theta)).toFixed(7)
            return [lng, lat];
        },
		
		// 小数转化
		decimal:function(value){
			if (!value) return '.00';
			let text = value ? (parseInt(value) / 100).toFixed(2) : '.00';
			return text.substring(text.indexOf("."),text.indexOf(".")+3);
		},
        /* 价格转换 */
        toPrice: function(value) {
            return value ? (parseInt(value/ 100)) : '0';
        },
		/* 阿拉伯数字转中文数字 */
		numFormat: function(v) {
			if (v === undefined || v === null || isNaN(v)) return;
			let arr = new Array("零", "一", "二", "三", "四", "五", "六", "七", "八", "九");
			return arr[Number(v)];
		},
    }
});
