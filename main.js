import Vue from 'vue'
import App from './App'
import tools from '@/common/tools.js';
import alert from '@/common/alert.js';
tools.alert = alert;
uni.tools = tools;
Vue.prototype.tools = tools;
Vue.prototype.$ajax = tools.ajax;
Vue.config.productionTip = false;
import share from '@/common/share.js'
Vue.mixin(share);/* 全局分享 */

App.mpType = 'app'

const app = new Vue({
    ...App
})
app.$mount()
